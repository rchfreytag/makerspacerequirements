Requirements for a Makerspace

* Space suitable for fitting mix of class, open, and shop spaces,

* 30,000+ sq ft
* Ceiling height: 14+'

* Loading dock
* Four street-level roll-up door 11' wide and 14' high.

* Single floor:     	Yes
* Power: 		3-phase 240V
* Natural gas:		Preferred
* HVAC:			Heating and Cooling
* Loading dock:		Yes
* Parking:		100 spaces with ability to block off for special uses.
* Singage:		Illuminated
* Roof:			Access, Flat for greenhouses, weather exposure
* Air rights: 		Yes

* Public Transportation Accessible

----------------------------

# Notes from #nl3wg_buildanddesign

* _20191005, Bill St. Amant 14:27_, blacksmiths want inside working space.
* _20191016, Sam Aparicio 21:50_, [Draft 1 of the Space Requirements document](https://docs.google.com/document/d/1OkOr3APg9tg2KfM-xOYqfR46VdceedUTn5vrzkxxtns/edit)
* _20191019, Craig Trader 16:38_, [Reston Now: One of Reston's First Office Developments is Approved for Major Redevelopment](https://www.restonnow.com/2019/10/18/one-of-restons-first-office-development-is-approved-for-major-redevelopment/)


# Notes from nl3wg_real_estate